#!/usr/bin/env python3

class graph:

    def __init__(self, vertex): 

        self.size = 0
        self.V = []
        self.E = [] 
        self.adj_matrix = [] 
        self.adj_list = []
        self.add_vertex(vertex)

    def add_vertex(self, vertex):
        
        if vertex in self.V:
            print("Já existe vértice com nome ", vertex, "no grafo.")
            return

        self.V.append(vertex)
        self.size += 1

        for line in self.adj_matrix:
            line.append(0)
        self.adj_matrix.append([0 for i in range(self.size)])

        self.adj_list.append([])
    
    def add_edge(self, edge):
        if edge[0] not in self.V or edge[1] not in self.V:
            print("Arestas só podem ser adicionadas se os dois vértices já existirem no grafo.")
            return
        if edge in self.E or [edge[1], edge[0]] in self.E:
            print("A aresta já existe no grafo.")
            return
       
        self.E.append(edge)
        
        if edge[0] == edge[1]:
            index = self.V.index(edge[0])
            self.adj_matrix[index][index] = 1
            self.adj_list[index].append(edge[0])
        else: 
            index_vertex_1 = self.V.index(edge[0])
            index_vertex_2 = self.V.index(edge[1])
            self.adj_matrix[index_vertex_1][index_vertex_2] = 1
            self.adj_matrix[index_vertex_2][index_vertex_1] = 1
            self.adj_list[index_vertex_1].append(edge[1])
            self.adj_list[index_vertex_2].append(edge[0])

    def remove_vertex(self, vertex):
        index = self.V.index(vertex)

        self.V.remove(vertex)

        del self.adj_matrix[index]
        for line in self.adj_matrix:
            del line[index]

        del self.adj_list[index]
        for line in self.adj_list:
            if vertex in line:
                line.remove(vertex)

    def remove_edge(self, edge):
        index_vertex_1 = self.V.index(edge[0])
        index_vertex_2 = self.V.index(edge[1])

        try:
            self.E.remove(edge)
        except:
            self.E.remove([edge[1], edge[0]])

        if edge[0]==edge[1]:
            index = self.V.index(edge[0])
            self.adj_matrix[index][index] = 0
            self.adj_list[index].remove(edge[0])
        else: 
            index_vertex_1 = self.V.index(edge[0])
            index_vertex_2 = self.V.index(edge[1])
            self.adj_matrix[index_vertex_1][index_vertex_2] = 0
            self.adj_matrix[index_vertex_2][index_vertex_1] = 0

            self.adj_list[index_vertex_1].remove(edge[1])
            self.adj_list[index_vertex_2].remove(edge[0])

